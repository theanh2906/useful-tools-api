import { NextApiRequest, NextApiResponse } from 'next';
import Cors from 'cors';

export const cors = Cors({
  methods: ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT', 'DELETE'],
});

export class Utils {
  static runMiddleware(
    req: NextApiRequest,
    res: NextApiResponse,
    fn: Function
  ) {
    return new Promise((resolve, reject) => {
      fn(req, res, (result: any) => {
        if (result instanceof Error) {
          return reject(result);
        }
        return resolve(result);
      });
    });
  }
  static async runCors(req: NextApiRequest, res: NextApiResponse) {
    return this.runMiddleware(req, res, cors);
  }
}
