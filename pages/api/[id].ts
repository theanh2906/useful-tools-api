// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { Browser, Page } from 'puppeteer';
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');
const fs = require('fs');
type Data = {
  name: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const id = req.query.id as string;
  main().then((data) => {
    res.status(200).json({ name: 'John Doe ' + id });
  });
}

const main = async () => {
  const browser: Browser = await puppeteer.launch({
    // headless: false,
    // defaultViewport: null,
    // args: ['--start-maximized'],
  });
  const page = await browser.newPage();
  await page.goto('https://javmodel.com/jav/homepages.php', {
    waitUntil: 'networkidle2',
  });
  const $ = cheerio.load(await page.content());
  const paginationSize = $('.pagination').find('a').length;
  const pageSize =
    $('.pagination').find('a')[paginationSize - 2].children[0].data;
  for (let i = 1; i <= 10; i++) {
    await page.goto(`https://javmodel.com/jav/homepages.php?page=${i}`);
    await runEachPage(page, browser);
  }
  // await page.pdf({ path: 'test.pdf', format: 'a4' });
  // await page.waitFor(5000);
};

const runEachPage = async (page: Page, browse: Browser) => {
  const $ = cheerio.load(await page.content());
  $('.team-box').each(async (index: number, element: Element) => {
    const modelLink = $(element).find('.team-photo > a').attr('href');
    const modelPage = await browse.newPage();
    await modelPage.goto(ROOT_URL + modelLink);
    await runEachLink(modelPage, ROOT_URL + modelLink);
    await modelPage.close();
  });
};

const runEachLink = async (modelPage: Page, link: string) => {
  const $ = cheerio.load(await modelPage.content());
  console.log(link);
};

const writeJSON = async (data: any, name: string) => {
  fs.writeFile(`${name}.json`, JSON.stringify(data, null, 2), (err: any) => {
    if (err) {
      console.log(err);
    } else {
      console.log('The file has been saved!');
    }
  });
};

const ROOT_URL = 'https://javmodel.com';
